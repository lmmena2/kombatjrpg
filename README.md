KombatJRPJ

Descripción

Juego de peleas entre dos players

Requisitos:

- Python 
- Django

Instalación

Clona el repositorio:

    https://gitlab.com/lmmena2/kombatjrpg.git

Configura el entorno virtual (recomendado):

    python -m venv venv

Activa el entorno virtual:

    Windows:   venv\Scripts\activate
    mac/linux: source venv/bin/activate


Configuración

Uso

Explica cómo ejecutar la aplicación: 

    python manage.py runserver

La aplicación ahora debería estar disponible en http://localhost:8000/KombatJRPG/fight/ en tu navegador web.

Si deseas cambiar los json de data de pelea, ingresa a view.py -> funcion fight_view 
