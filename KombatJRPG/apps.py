from django.apps import AppConfig


class KombatjrpgConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'KombatJRPG'
