from django.urls import path
from . import views

urlpatterns = [
    path('fight/', views.fight_view, name='fight'),
]