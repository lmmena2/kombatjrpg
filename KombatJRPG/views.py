from django.shortcuts import render
from django.http import HttpResponse

class Character:
    def __init__(self, name, energy=6):
        self.name = name
        self.energy = energy

    def is_alive(self):
        return self.energy > 0

    def take_damage(self, damage):
        self.energy = max(0, self.energy - damage)

    def attack(self, opponent, combo_name):
        if combo_name in self.combos:
            moves, damage = self.combos[combo_name]
            combo_attack = "puñetazo" if "P" in combo_name else "patada"
            combo_moves = "".join(moves)
            aux_name = ""
            if combo_name == "DSDP" or combo_name == "ASAP":
                aux_name = "Taladoken"
            if combo_name == "SDK" or combo_name == "SAK":
                aux_name = "Remuyuken"

            attack_detail = f"{self.name} golpea con un {aux_name}, le quita {damage} de energía a {opponent.name}"
            if damage > 0:
                opponent.take_damage(damage)
            return attack_detail
        else:
            combo_attack = "patada" if "K" in combo_name else "puñetazo"
            if combo_name and combo_name != "":
                attack_detail = f"{self.name} golpea con {combo_attack} simple a {opponent.name}, le quita 1 de energía"
                opponent.take_damage(1)
                return attack_detail

def build_combo_sequence(moves, attacks):
    combo_sequence = []
    for move, attack in zip(moves, attacks):
        if attack:
            combo_sequence.append(move + attack)
    return combo_sequence

def fight_view(request):
    json_data = {
        "player1": {
            "movimientos": ["D", "DSD", "S", "DSD", "SD"],
            "golpes": ["K", "P", "", "K", "P"]
        },
        "player2": {
            "movimientos": ["SA", "SA", "SA", "ASA", "SA"],
            "golpes": ["K", "", "K", "P", "P"]
        }
    }

    player1_combos = {
        "DSDP": (["D", "S", "D"], 3),
        "SDK": (["S", "D"], 2)
    }

    player2_combos = {
        "SAK": (["S", "A"], 3),
        "ASAP": (["A", "S", "A"], 2),
    }

    player1 = Character("Tonyn Stallone")
    player1.combos = player1_combos

    player2 = Character("Arnaldor Shuatseneguer")
    player2.combos = player2_combos

    combo_sequence_player1 = build_combo_sequence(json_data["player1"]["movimientos"], json_data["player1"]["golpes"])
    combo_sequence_player2 = build_combo_sequence(json_data["player2"]["movimientos"], json_data["player2"]["golpes"])

    max_combos = max(len(combo_sequence_player1), len(combo_sequence_player2))

    # Lista para almacenar los detalles de la pelea
    fight_details_list = []

    for i in range(max_combos):
        if i < len(combo_sequence_player1):
            detail = player1.attack(player2, combo_sequence_player1[i])
            fight_details_list.append(detail)
            if not player2.is_alive():
                break

        if i < len(combo_sequence_player2):
            detail = player2.attack(player1, combo_sequence_player2[i])
            fight_details_list.append(detail)
            if not player1.is_alive():
                break

    winner = player1 if player1.is_alive() else player2
    fight_details_list.append(f"{winner.name} Gana la pelea y aún le queda {winner.energy} de energía")

    # Preparamos los datos para pasar a la plantilla
    context = {
        'player1_name': player1.name,
        'player1_energy': player1.energy,
        'player2_name': player2.name,
        'player2_energy': player2.energy,
        'fight_details_list': fight_details_list,  # Pasamos la lista de detalles de la pelea al contexto
    }

    # Utilizamos render para cargar la plantilla
    return render(request, 'KombatJRPG/fight.html', context)
